### Repository Search

#### Installation

Application dependencies should all be able to be pulled down via:

```bash
npm install
```

#### Running

Running the application using Webpack Dev Server can be done by running:

```bash
npm start
```

Building for production/hosting on an actual server can be done by running:

```bash
npm run build
```

The output bundle will be in the `./dist` directory.

#### Tests

Initially, I had started TDD on the tests and components but got to a point where I needed to move a bunch of things around due to improvements and design changes.
In the interest of time I decided to leave the tests where they are at. They're not complete and there are some errors that need to be fixed. Due to time constraints I need to leave the tests where they are at.
