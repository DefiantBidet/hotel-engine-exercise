import { v4 } from 'uuid';

import * as API from 'API';

export function createOwner(mockOwner?: Partial<API.Owner>): API.Owner {
  const mockLogin = mockOwner?.login ?? 'fooby'

  const owner: API.Owner = {
    avatarUrl: null,
    eventsUrl: `https://api.github.com/users/${mockLogin}/events{/privacy}`,
    followersUrl: `https://api.github.com/users/${mockLogin}/followers`,
    followingUrl: `https://api.github.com/users/${mockLogin}/following{/other_user}`,
    gistsUrl: `https://api.github.com/users/${mockLogin}/gists{/gistId}`,
    gravatarId: null,
    htmlUrl: `https://github.com/${mockLogin}`,
    id: 9999,
    login: mockLogin,
    nodeId: null,
    organizationsUrl: `https://api.github.com/users/${mockLogin}/orgs`,
    receivedEventsUrl: `https://api.github.com/users/${mockLogin}/received_events`,
    reposUrl: `https://api.github.com/users/${mockLogin}/repos`,
    siteAdmin: false,
    starredUrl: `https://api.github.com/users/${mockLogin}/starred{/owner}{/repo}`,
    subscriptionsUrl: `https://api.github.com/users/${mockLogin}/subscriptions`,
    type: 'User',
    url: `https://api.github.com/users/${mockLogin}`,
    ...mockOwner
  };
  return owner;
};

export function createSearchItem(mockSearchItem?: Partial<API.SearchItem>): API.SearchItem {
  const mockName = mockSearchItem?.name ?? 'foo'
  const mockOwner = mockSearchItem?.owner || createOwner();
  const mockDate = new Date().toISOString();

  const item: API.SearchItem = {
    allowForking: true,
    archived: false,
    archiveUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/{archive_format}{/ref}`,
    assigneesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/assignees{/user}`,
    blobsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/git/blobs{/sha}`,
    branchesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/branches{/branch}`,
    cloneUrl: `https://github.com/${mockOwner.login}/${mockName}.git`,
    collaboratorsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/collaborators{/collaborator}`,
    commentsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/comments{/number}`,
    commitsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/commits{/sha}`,
    compareUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/compare/{base}...{head}`,
    contentsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/contents/{+path}`,
    contributorsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/contributors`,
    createdAt: mockDate,
    defaultBranch: 'master',
    deploymentsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/deployments`,
    description: 'foo bar baz',
    disabled: false,
    downloadsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/downloads`,
    eventsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/events`,
    fork: false,
    forks: 2310,
    forksCount: 2310,
    forksUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/forks`,
    fullName: `${mockOwner.login}/${mockName}`,
    gitCommitsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/git/commits{/sha}`,
    gitRefsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/git/refs{/sha}`,
    gitTagsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/git/tags{/sha}`,
    gitUrl: `git://github.com/${mockOwner.login}/${mockName}.git`,
    hasDownloads: true,
    hasIssues: true,
    hasPages: false,
    hasProjects: true,
    hasWiki: true,
    homepage: null,
    hooksUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/hooks`,
    htmlUrl: `https://github.com/${mockOwner.login}/${mockName}`,
    id:  999999,
    issueCommentUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/issues/comments{/number}`,
    issueEventsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/issues/events{/number}`,
    issuesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/issues{/number}`,
    isTemplate: false,
    keysUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/keys{/keyId}`,
    labelsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/labels{/name}`,
    language: 'JavaScript',
    languagesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/languages`,
    license: null,
    mergesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/merges`,
    milestonesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/milestones{/number}`,
    mirrorUrl: null,
    name: mockName,
    nodeId: v4(),
    notificationsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/notifications{?since,all,participating}`,
    openIssues: 0,
    openIssuesCount: 0,
    owner: mockOwner,
    private: false,
    pullsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/pulls{/number}`,
    pushedAt: mockDate,
    releasesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/releases{/id}`,
    score: 1,
    size: 1234,
    sshUrl: `git@github.com:${mockOwner.login}/${mockName}.git`,
    stargazersCount: 987,
    stargazersUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/stargazers`,
    statusesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/statuses/{sha}`,
    subscribersUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/subscribers`,
    subscriptionUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/subscription`,
    svnUrl: `https://github.com/${mockOwner.login}/${mockName}`,
    tagsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/tags`,
    teamsUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/teams`,
    topics: [],
    treesUrl: `https://api.github.com/repos/${mockOwner.login}/${mockName}/git/trees{/sha}`,
    updatedAt: mockDate,
    url: `https://api.github.com/repos/${mockOwner.login}/${mockName}`,
    visibility: "public",
    watchers: 123,
    watchersCount: 123,
    ...mockSearchItem,
  };
  return item;
};

export function createSearchItems(count: number = 1): API.SearchItem[] {
  const items: API.SearchItem[] = [];

  for(let i: number = 0; i < count; i++) {
    items.push(createSearchItem({owner: createOwner()}));
  }

  return items;
}
