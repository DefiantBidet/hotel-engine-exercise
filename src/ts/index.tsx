import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import RepositoryProvider from 'Contexts/RepositoryContext';
import SearchPage from 'Pages/Search';
import ResultsPage from 'Pages/Results';

const appElement = document.querySelector('main#app');

ReactDOM.render(
  <>
    <RepositoryProvider>
      <BrowserRouter>
        <Routes>
          <Route index element={<SearchPage />} />
          <Route path="/results/:id" element={<ResultsPage />} />
        </Routes>
      </BrowserRouter>
    </RepositoryProvider>
  </>,
  appElement,
);
