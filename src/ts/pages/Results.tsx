import { useContext } from 'react';

import { RepositoryContext } from 'Contexts/RepositoryContext';
import styles from 'Styles/pages/results.scss';

/**
 * Results UI
 * Shows basic information of selected repo.
 * @return {JSX.Element}
 * @function
 */
export default function ResultsPage(): JSX.Element {
  const { selectedRepo } = useContext(RepositoryContext);
  // no longer needed due to context - intent was if we needed fetch on this route
  // const params = useParams();
  // const repoId = params.id;

  const renderRepoDetails = (): JSX.Element => {
    return (
      <>
        <div className={styles.detailsContainer}>
          <div className={styles.detailsRow}>
            <div className={styles.fieldTitle}>
              Repo Name:
            </div>
            <div className={styles.fieldValue}>
              {selectedRepo?.name}
            </div>
          </div>
          <div className={styles.detailsRow}>
            <div className={styles.fieldTitle}>
              Repo Description:
            </div>
            <div className={styles.fieldValue}>
              {selectedRepo?.description}
            </div>
          </div>
          <div className={styles.detailsRow}>
            <div className={styles.fieldTitle}>
              Stars:
            </div>
            <div className={styles.fieldValue}>
              {selectedRepo?.stargazers_count}
            </div>
          </div>
          <div className={styles.detailsRow}>
            <div className={styles.fieldTitle}>
              Language:
            </div>
            <div className={styles.fieldValue}>
              {selectedRepo?.language}
            </div>
          </div>
          <div className={styles.detailsRow}>
            <div className={styles.fieldTitle}>
              Owner:
            </div>
            <div className={styles.fieldValue}>
              {selectedRepo?.owner.login}
            </div>
          </div>
        </div>
      </>
    );
  };

  return (
    <>
      <div className={styles.pageContainer}>
        {selectedRepo && renderRepoDetails()}
      </div>
    </>
  );
}
