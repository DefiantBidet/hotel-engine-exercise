import {render} from '@testing-library/react'

import SearchPage from './Search';

describe('<SearchPage />', () => {
  test('creates a container ', () => {
    render(<SearchPage />);
    const element = document.querySelector('div.pageContainer');
    expect(element).toBeInTheDocument();
  });
});
