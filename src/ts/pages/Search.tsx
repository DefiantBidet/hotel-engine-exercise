import { AxiosResponse } from 'axios';
import { useContext, useState } from 'react';

import * as API from 'API';
import { search } from 'Api/search';
import { RepositoryContext } from 'Contexts/RepositoryContext';
import SearchForm from 'Components/SearchForm';
import FilterResults from 'Components/SearchResults/Filter';
import SortResults from 'Components/SearchResults/Sort';
import SearchResults from 'Components/SearchResults';
import { SortType, SortTypes } from 'Types/sort';

import styles from 'Styles/pages/search.scss';

/**
 * Search UI
 * Shows search form and results of search query
 * @return {JSX.Element}
 * @function
 */
// export default function SearchPage(props: SearchPageProps): JSX.Element {
export default function SearchPage(): JSX.Element {
  const { dataset, setDataset } = useContext(RepositoryContext);
  const [searchString, setSearchString] = useState<string>('');
  const [sort, setSort] = useState<SortType>(SortTypes.Best);
  const [items, setItems] = useState<API.SearchItem[]>([]);

  const resetItems = (dataset: API.SearchResponse | null): API.SearchItem[] => {
    if (dataset !== null) {
      return dataset.items;
    }

    return [];
  };

  const onSort = async (value: string): Promise<void> => {
    const sortType: SortType = SortTypes[value as keyof typeof SortTypes];

    let response: AxiosResponse<API.SearchResponse, any>;

    // 'best' is default and doesn't need params,
    if (sortType === SortTypes.Best) {
      response = await search(searchString);

    } else {
      response = await search(searchString, value.toLowerCase());
    }

    setSort(sortType);
    setDataset(response.data);
    setItems(resetItems(response.data));
  };

  const onFilterResults = (value: string): void => {
    if (value.length > 0) {
      const filteredItems = dataset!.items.filter((item: API.SearchItem) => {
        const language = item.language ?? '';
        return language.toLowerCase() === value.toLowerCase()
      });
      setItems(filteredItems);

    } else {
      setItems(resetItems(dataset));
    }
  };

  const onFormSubmit = async (value: string): Promise<void> => {
    setSearchString(value);
    const response = await search(value);

    setDataset(response.data);
    setItems(resetItems(response.data));
  };

  return (
    <>
      <div className={styles.pageContainer}>
        <div className={styles.searchContainer}>
          <SearchForm onSubmit={onFormSubmit} />
        </div>
        <div className={styles.resultsContainer}>
          {items.length > 0 && <SortResults sortKey={sort} setSort={onSort} />}
          {items.length > 0 && (
            <FilterResults items={dataset?.items ?? []} onFilter={onFilterResults} />
          )}
          <SearchResults results={items} />
        </div>
      </div>
    </>
  );
}
