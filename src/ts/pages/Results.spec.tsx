import {render} from '@testing-library/react';
// import { render, fireEvent, waitForElement } from "@testing-library/react";

import ResultsPage, { ResultsPageProps } from "./Results";


const renderSearch = (props: Partial<ResultsPageProps> = {}) => {
  const el = document.createElement('testing-fixture');

  return render(<ResultsPage {...props} />, {
    container: document.body.appendChild(el),
  });
};

describe('<ResultsPage />', () => {
  test('implement this test!', async () => {
    const { container } = renderSearch();
    expect(container).toBeInTheDocument();
  });
});
