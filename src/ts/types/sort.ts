

export enum SortTypes {
  Best = 'Best Match',
  Stars = 'Stars',
}

export type SortType = SortTypes.Best | SortTypes.Stars;

export interface SortResultOption {
  label: string;
  checked: boolean;
  value: string;
};
