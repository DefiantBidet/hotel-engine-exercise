declare module 'API' {
  export interface License {
    key: string | null;
    name: string | null;
    node_id: string | null;
    spdx_id: string | null;
    url: string | null;
  }

  export interface Owner {
    avatar_url: string | null;
    events_url: string | null;
    followers_url: string | null;
    following_url: string | null;
    gists_url: string | null;
    gravatar_id: string | null;
    html_url: string | null;
    id: number;
    login: string | null;
    node_id: string | null;
    organizations_url: string | null;
    received_events_url: string | null;
    repos_url: string | null;
    site_admin: boolean;
    starred_url: string | null;
    subscriptions_url: string | null;
    type: string | null;
    url: string | null;
  }

  export interface SearchItem {
    allow_forking: boolean;
    archived: boolean;
    archive_url: string | null;
    assignees_url: string | null;
    blobs_url: string | null;
    branches_url: string | null;
    clone_url: string | null;
    collaborators_url: string | null;
    comments_url: string | null;
    commits_url: string | null;
    compare_url: string | null;
    contents_url: string | null;
    contributors_url: string | null;
    created_at: string | null;
    default_branch: string | null;
    deployments_url: string | null;
    description: string | null;
    disabled: boolean;
    downloads_url: string | null;
    events_url: string | null;
    fork: boolean;
    forks: number;
    forks_count: number;
    forks_url: string | null;
    full_name: string | null;
    git_commits_url: string | null;
    git_refs_url: string | null;
    git_tags_url: string | null;
    git_url: string | null;
    has_downloads: boolean;
    has_issues: boolean;
    has_pages: boolean;
    has_projects: boolean;
    has_wiki: boolean;
    homepage: string | null;
    hooks_url: string | null;
    html_url: string | null;
    id: number;
    issue_comment_url: string | null;
    issue_events_url: string | null;
    issues_url: string | null;
    is_template: boolean;
    keys_url: string | null;
    labels_url: string | null;
    language: string | null;
    languages_url: string | null;
    license: License | null;
    merges_url: string | null;
    milestones_url: string | null;
    mirror_url: string | null;
    name: string | null;
    node_id: string | null;
    notifications_url: string | null;
    open_issues: number;
    open_issues_count: number;
    owner: Owner;
    private: boolean;
    pulls_url: string | null;
    pushed_at: string | null;
    releases_url: string | null;
    score: number;
    size: number;
    ssh_url: string | null;
    stargazers_count: number;
    stargazers_url: string | null;
    statuses_url: string | null;
    subscribers_url: string | null;
    subscription_url: string | null;
    svn_url: string | null;
    tags_url: string | null;
    teams_url: string | null;
    topics: string[];
    trees_url: string | null;
    updated_at: string | null;
    url: string | null;
    visibility: string | null;
    watchers: number;
    watchers_count: number;
  }

  export interface SearchResponse {
    items: SearchItem[];
    incomplete_results?: boolean;
    total_count?: number;
  }
}
