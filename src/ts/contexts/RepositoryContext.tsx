import React, { createContext, useState } from 'react';

import * as API from 'API';

const noop = () => { /* noop */ };

export interface IRepositoryContext {
  selectedRepo: API.SearchItem | null;
  dataset: API.SearchResponse | null;
  setSelectedRepo: (selected: API.SearchItem) => void;
  setDataset: (dataset: API.SearchResponse) => void;
}

const initialContext = {
  selectedRepo: null,
  setSelectedRepo: noop,
  dataset: null,
  setDataset: noop,
};

export const RepositoryContext = createContext<IRepositoryContext>(initialContext);

const RepositoryProvider: React.FC<React.ReactNode> = ({ children }) => {
  const [repo, setRepo] = useState<API.SearchItem | null>(null);
  const [repoList, setRepoList] = useState<API.SearchResponse | null>(null);

  const providerValues: IRepositoryContext = {
    selectedRepo: repo,
    setSelectedRepo: setRepo,
    dataset: repoList,
    setDataset: setRepoList,
  };

  return <RepositoryContext.Provider value={providerValues}>{children}</RepositoryContext.Provider>;
};

export default RepositoryProvider;
