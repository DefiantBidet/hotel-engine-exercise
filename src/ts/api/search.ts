import axios, { AxiosResponse } from 'axios';

import * as API from 'API'

const baseURL = 'https://api.github.com';

const githubHeaders = {
  'Accept': 'application/vnd.github.v3+json',
};

const fetch = axios.create({
  baseURL: `${baseURL}`,
  headers: githubHeaders,
});

/**
 * GET - Fetches repo information from GitHub API
 * @param  {string}  query query query to search
 * @param  {string}  sort optional sort parameter
 * @return {Promise}     Axios Response Data
 * @async
 * @function
 */
export const search = async (query: string, sort?: string): Promise<AxiosResponse<API.SearchResponse, any>> => {
  let queryString = `q=${query}`;
  if (sort) {
    queryString += `&sort=${sort}`;
  }
  const params: URLSearchParams = new URLSearchParams(queryString);

  return fetch.get<API.SearchResponse>('/search/repositories', { params });
};
