import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import TextInput, { TextInputProps } from "./index";

describe('<TextInput />', () => {
  const mockProps: TextInputProps = {
    elementName: 'foo',
    placeHolder: 'Enter Foo',
  };

  test('creates a fieldset', () => {
    render(<TextInput {...mockProps} />);
    const fieldset = document.querySelector('fieldset[class="inputFieldset"]');
    expect(fieldset).toBeInTheDocument();
  });

  test('creates input', () => {
    render(<TextInput {...mockProps} />);
    const inputElement = document.querySelector(`#${mockProps.elementName}`);
    expect(inputElement).toBeInTheDocument();
  });

  test('creates placeholder label', () => {
    render(<TextInput {...mockProps} />);
    const placeholderLabel = screen.getByLabelText(mockProps.placeHolder);
    expect(placeholderLabel).toBeInTheDocument();
  });

  test('placeholder is not present with text entered', () => {
    render(<TextInput {...mockProps} />);
    const mockText = 'foo';
    const inputElement: HTMLInputElement = screen.getByRole('textbox', { name: mockProps.placeHolder });
    // const placeholderLabel = screen.getByLabelText(mockProps.placeHolder, { selector: 'label' });
    const placeholderLabel = document.querySelector('label[class="placeholder"]');

    expect(inputElement.value).toBe('');

    userEvent.type(inputElement, mockText)

    expect(inputElement.value).toBe(mockText);
    expect(placeholderLabel).not.toBeInTheDocument();
  })
});
