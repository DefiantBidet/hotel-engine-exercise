import { useEffect, useState } from 'react';

import styles from 'Styles/components/textInput.scss';

/**
 * Props for TextInput
 */
export interface TextInputProps {
  elementName: string;
  inputValue?: string;
  onInput?: (value: string) => void;
  placeHolder: string;
};

/**
 * TextInput renders a text input field
 * @param {TextInputProps} props Props of Component
 * @return {JSX.Element}
 * @function
 */
export default function TextInput(props: TextInputProps): JSX.Element {
  const [showPlaceHolder, setShowPlaceholder] = useState<boolean>(true);
  const [inputValue, setInputValue] = useState<string>(props.inputValue ?? '');

  useEffect(() => {
    setShowPlaceholder(isPlaceHolderVisible());
  }, [inputValue]);

  /**
   * onBlur event handler
   * @param  {React.ChangeEvent<HTMLInputElement>} event Blur Event
   * @return {void}
   * @function
   */
  const onBlur = (event: React.ChangeEvent<HTMLInputElement>): void => {
    event.stopPropagation();
    event.preventDefault();

    const trimmedValue = inputValue.trim();
    // handle trimmed string being different from supplied
    if (inputValue !== trimmedValue) {
      setInputValue(trimmedValue);
    }

    setShowPlaceholder(isPlaceHolderVisible());
  };

  /**
   * onFocus event handler
   * @param  {React.ChangeEvent<HTMLInputElement>} event Focus Event
   * @return {void}
   * @function
   */
  const onFocus = (event: React.ChangeEvent<HTMLInputElement>): void => {
    // calling stopPropagation in the capture phase prevents cursor display
    // @see https://bugzilla.mozilla.org/show_bug.cgi?id=509684
    // event.stopPropagation();
    event.preventDefault();

    setShowPlaceholder(isPlaceHolderVisible());
  };

  /**
   * onInput event handler
   * @param  {React.ChangeEvent<HTMLInputElement>} event Input Event
   * @return {void}
   * @function
   */
  const onInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
    event.stopPropagation();
    event.preventDefault();

    const { value } = event.target;

    setInputValue(value);
    setShowPlaceholder(isPlaceHolderVisible())

    if (props.onInput) {
      props.onInput(value);
    }
  };

  /**
   * determine if input placeholder is present
   * @return {boolean}
   * @function
   */
  const isPlaceHolderVisible = (): boolean => inputValue.length === 0;

  /**
   * Creates Input Placeholder
   * @return {JSX.Element | null}
   * @function
   */
  const createPlaceholder = (): JSX.Element | null => {
    const { elementName, placeHolder } = props;

    if (!showPlaceHolder) {
      return null;
    }

    return (
      <label
        aria-hidden="true"
        className={styles.placeHolder}
        htmlFor={elementName}
      >
        {placeHolder}
      </label>
    );
  }

  /**
   * Creates Input Element
   * @return {JSX.Element}
   * @function
   */
  const createInput = (): JSX.Element => {
    const { elementName, placeHolder } = props;

    return (
      <input
        aria-required={true}
        aria-label={placeHolder}
        id={elementName}
        className={styles.input}
        name={elementName}
        onBlur={onBlur}
        onFocus={onFocus}
        onInput={onInput}
        type="text"
        value={inputValue}
      />
    );
  }

  return (
    <>
      <fieldset className={styles.inputFieldset}>
        {createInput()}
        {showPlaceHolder && createPlaceholder()}
      </fieldset>
    </>
  );
};
