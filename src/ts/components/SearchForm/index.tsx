import { useState } from 'react';

import styles from 'Styles/components/searchForm.scss';
import TextInput from './TextInput';

/**
 * Props for SearchForm
 */
export interface SearchFormProps {
  onSubmit?: (value: string) => Promise<void>;
}

/**
 * SearchForm renders a Form to hold the input for searching github
 * @param {SearchFormProps} props  Props of the Component
 * @return {JSX.Element}
 * @function
 */
export default function SearchForm(props: SearchFormProps): JSX.Element {
  const [inputValue, setInputValue] = useState<string>('');

  /**
   * Input change handler
   * @param  {string} value Input value
   * @return {void}
   * @function
   */
  const onInputChange = (value: string): void => {
    setInputValue(value);
  };

  /**
   * Form submit handler
   * @return {Promise<void>}
   * @async
   * @function
   */
  const onSubmit = async (event: React.FormEvent): Promise<void> => {
    event.stopPropagation();
    event.preventDefault();

    if (!inputValue.trim().length) {
      // no value - don't submit
      return;
    }

    if (props.onSubmit) {
      props.onSubmit(inputValue);
    }
  };

  return (
    <>
      <form
        className={styles.pageContainer}
        onSubmit={onSubmit}
        aria-labelledby="search-repositories"
      >
        <fieldset>
          <legend id="search-repositories" className={styles.legend}>
            Search
          </legend>
          <TextInput
            elementName="search-repo-input"
            placeHolder="Search Repositories"
            onInput={onInputChange}
          />
        </fieldset>
      </form>
    </>
  );
}
