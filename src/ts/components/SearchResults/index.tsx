
import * as API from 'API';
import styles from 'Styles/components/searchResults.scss';
import ResultCard from './ResultCard';

/**
 * Props for SearchResults
 */
export interface SearchResultsProps {
  results: API.SearchItem[] | null;
}

/**
 * SearchResults UI
 * Displays a list of results from search query
 * @return {JSX.Element}
 * @function
 */
export default function SearchResults(props: SearchResultsProps): JSX.Element | null {
  const { results } = props;

  if (results === null) {
    return null;
  }

  if (results.length === 0) {
    return null;
  }

  return (
    <>
      <ul className={styles.pageContainer}>
        {results.map((item: API.SearchItem, index: number) => (
          <li className={styles.listItem} key={`list_item-search_item-${index}`}>
            <ResultCard searchItem={item} />
          </li>
        ))}
      </ul>
    </>
  );
}
