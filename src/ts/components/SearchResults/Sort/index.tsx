import { useEffect, useState } from 'react';

import { SortResultOption, SortTypes } from 'Types/sort';

/**
 * Props for TextInput
 */
export interface SortResultsProps {
  sortKey: string;
  setSort?: (value: string) => void;
}

/**
 * SortResults UI
 * Renders radio buttons to sort repo list
 * @return {JSX.Element}
 * @function
 */
export default function SortResults(props: SortResultsProps): JSX.Element {
  const [sortOptions, setSortOptions] = useState<SortResultOption[]>([]);

  useEffect(() => {
    const sortOptions: SortResultOption[] = [];
    for (const [key, value] of Object.entries(SortTypes)) {
      const option: SortResultOption = {
        label: value,
        checked: value === props.sortKey,
        value: key,
      };
      sortOptions.push(option);
    }
    setSortOptions(sortOptions);
  }, [props.sortKey]);

  const onSort = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const value = event.target.value;
    if (props.setSort) {
      props.setSort(value);
    }
  };

  return (
    <>
      <div className="">
        {sortOptions.map((option: SortResultOption, index: number) => (
          <label key={`radio-sort-${index}`}>
            <input type="radio" value={option.value} checked={option.checked} onChange={onSort} />
            {option.label}
          </label>
        ))}
      </div>
    </>
  );
}
