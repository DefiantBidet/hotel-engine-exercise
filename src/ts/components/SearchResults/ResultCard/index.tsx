import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';

import * as API from 'API';

import { RepositoryContext } from 'Contexts/RepositoryContext';
import styles from 'Styles/components/resultsCard.scss';

/**
 * Props for ResultsPage
 */
export interface ResultsCardProps {
  searchItem: API.SearchItem;
}

/**
 * Results UI
 * Renders a Card to display minimal repo details
 * @return {JSX.Element}
 * @function
 */
export default function ResultsCard(props: ResultsCardProps): JSX.Element {
  const { setSelectedRepo } = useContext(RepositoryContext);
  const navigate = useNavigate();

  const { id, name, owner, stargazers_count } = props.searchItem;
  const onCardClick = (event: React.MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    setSelectedRepo(props.searchItem);
    navigate(`/results/${id}`, { replace: true });
  };

  return (
    <>
      <div className={styles.pageContainer}>
        <a onClick={onCardClick} className={styles.anchor}>
          <div className={styles.repoRow}>
            <p className={styles.rowText}>{name}</p>
            <p className={styles.rowText}>{stargazers_count}</p>
          </div>
          <div className={styles.authorRow}>
            <p className={styles.rowText}>{owner.login}</p>
          </div>
        </a>
      </div>
    </>
  );
}
