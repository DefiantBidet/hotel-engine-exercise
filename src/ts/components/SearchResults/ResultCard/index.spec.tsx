import { render, screen } from '@testing-library/react'

import { createOwner, createSearchItem  } from 'Test/utils/index';
import ResultCard from './index';


describe('<ResultCard />', () => {
  test('creates a container', () => {
    render(<ResultCard searchItem={createSearchItem()} />);
    const element = document.querySelector('div.pageContainer');
    expect(element).toBeInTheDocument();
  });

  test('creates an anchor element', () => {
    render(<ResultCard searchItem={createSearchItem()} />);
    const element = document.querySelector('a.anchor');
    expect(element).toBeInTheDocument();
  });

  test('displays name of repo', () => {
    const mockName = 'my-awesome-project'
    const mockData = createSearchItem({ name: mockName });

    render(<ResultCard searchItem={mockData} />);

    const nameElement = screen.getByText(mockName);
    expect(nameElement).toBeInTheDocument();
  });

  test('displays total stars of repo', () => {
    const mockStarCount = 350;
    const mockData = createSearchItem({ stargazersCount: mockStarCount });

    render(<ResultCard searchItem={mockData} />);

    const nameElement = screen.getByText(mockStarCount);
    expect(nameElement).toBeInTheDocument();
  });

  test('displays author of repo', () => {
    const mockName = 'foobymcbarbaz';
    const mockOwner = createOwner({ login: mockName });
    const mockData = createSearchItem({ owner: mockOwner });

    render(<ResultCard searchItem={mockData} />);

    const nameElement = screen.getByText(mockName);
    expect(nameElement).toBeInTheDocument();
  });
});
