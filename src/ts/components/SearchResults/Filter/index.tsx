
import Select, { ActionMeta, OnChangeValue } from 'react-select';

import * as API from 'API';


interface SelectOption {
  label: string;
  value: string;
}

/**
 * Filters the repo languages from the current list of items
 * @param list
 * @returns array of selected options
 */
function filterLanguages(list: API.SearchItem[]): SelectOption[] {
  const languages: Record<string, number> = {};
  list.forEach((item: API.SearchItem) => {
    if (!item.language) {
      return;
    }

    if (languages[item.language]) {
      // lang exists, bump count
      languages[item.language] += 1;
      return;
    }
    // lang doesn't exit, add it
    languages[item.language] = 1;
  });

  const entries = Object.entries(languages)
    .sort(([, a], [, b]) => b - a)
    .map((lang) => lang[0]);

  return entries.map(
    (language: string): SelectOption => ({
      label: language,
      value: language,
    }),
  );
}

/**
 * Props for TextInput
 */
export interface FilterResultsProps {
  items: API.SearchItem[];
  onFilter?: (value: string) => void;
}

/**
 * FilterResults UI
 * Renders a React Select to display filtered languages
 * @return {JSX.Element}
 * @function
 */
export default function FilterResults(props: FilterResultsProps): JSX.Element {
  const onSelectOption = (
    newValue: OnChangeValue<SelectOption, false>,
    actionMeta: ActionMeta<SelectOption>,
  ) => {
    if (!props.onFilter){
      return;
    }

    if (newValue && actionMeta.action === 'select-option') {
      props.onFilter(newValue.value);
    }

    if (actionMeta.action === 'clear') {
      props.onFilter('');
    }
  };

  return (
    <Select
      isClearable={true}
      placeholder="Select Language"
      options={filterLanguages(props.items)}
      onChange={onSelectOption}
    />
  );
}
