import { render } from '@testing-library/react';

import * as API from 'API';
import { createSearchItems } from 'Test/utils/index';
import SearchResults from './index';

describe('<SearchResults />', () => {
  const mockResults: API.SearchResponse = {
    incomplete_results: false,
    items: createSearchItems(3),
    total_count: 3,
  };

  test('creates an unordered list ', () => {
    render(<SearchResults results={mockResults} />);
    const ulElement = document.querySelector('ul.pageContainer');
    expect(ulElement).toBeInTheDocument();
  });

  test('creates a list item for each item in list', () => {
    render(<SearchResults results={mockResults}/>);
    const ulElement = document.querySelector('ul.pageContainer');
    expect(ulElement).toBeInTheDocument();
  });

  test('does not render list if no \'results\' prop supplied', () => {
    render(<SearchResults results={null} />);
    const ulElement = document.querySelector('ul.pageContainer');
    expect(ulElement).not.toBeInTheDocument();
  });

  test('does not render list if no \'items\' in supplied prop', () => {
    const results = mockResults;
    results.items = [];
    render(<SearchResults results={mockResults} />);
    const ulElement = document.querySelector('ul.pageContainer');
    expect(ulElement).not.toBeInTheDocument();
  });
});
